# frozen_string_literal: true
module Mirros
  module Widget
    module EmbedPdf
      class PdfUpload < Upload
        validate :file_format

        private

        def file_format
          return unless file.attached?
          return if file.blob.content_type.eql? 'application/pdf'
          file.purge_later
          errors.add(:file, I18n.t('embed_pdf.invalid_content_type', content_type: file.blob.content_type))
        end
      end
    end
  end
end

