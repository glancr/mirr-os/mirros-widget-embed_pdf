# frozen_string_literal: true
module Mirros
  module Widget
    module EmbedPdf
      class Configuration < WidgetInstanceConfiguration
        attribute :pdf_file_id, :string, default: ''
        attribute :remote_files, :boolean, default: false
        attribute :remote_pdf_file_url, :string, default: ''

        validates :remote_files, boolean: true
      end
    end
  end
end
