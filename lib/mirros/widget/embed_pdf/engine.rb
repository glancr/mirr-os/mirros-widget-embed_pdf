module Mirros
  module Widget
    module EmbedPdf
      class Engine < ::Rails::Engine
        isolate_namespace Mirros::Widget::EmbedPdf
        config.generators.api_only = true
      end
    end
  end
end
