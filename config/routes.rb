# frozen_string_literal: true

module Mirros
  module Widget
    EmbedPdf::Engine.routes.draw do
      resources :pdf_uploads
    end
  end
end
