require 'json'
$:.push File.expand_path('../lib', __FILE__)

# Maintain your gem's version:
require "mirros/widget/embed_pdf/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |spec|
  spec.name        = "mirros-widget-embed_pdf"
  spec.version     = Mirros::Widget::EmbedPdf::VERSION
  spec.authors     = ["Tobias Grasse"]
  spec.email       = ["tg@glancr.de"]
  spec.homepage    = ""
  spec.summary     = "Display a PDF file on your board."
  spec.description = "Upload or link to any PDF file to show it on your board."
  spec.license     = "MIT"
  spec.metadata    = { 'json' =>
    {
      type: 'widgets',
      title: {
        enGb: 'PDF',
        deDe: 'PDF',
        frFr: 'PDF',
        esEs: 'PDF',
        plPl: 'PDF',
        koKr: 'PDF',
      },
      description: {
        enGb: spec.description,
        deDe: 'Lade ein PDF hoch oder verlinke es, um es auf auf deinem Board anzuzeigen.',
        frFr: 'Téléchargez ou créez un lien vers n\'importe quel fichier PDF pour l\'afficher sur votre tableau.',
        esEs: 'Sube o enlaza a cualquier archivo PDF para mostrarlo en tu pizarra.',
        plPl: 'Prześlij lub połącz się z dowolnym plikiem PDF, aby pokazać go na swojej tablicy.',
        koKr: '보드에 표시하려면 PDF 파일을 업로드하거나 링크하세요.'
      },
      group: nil, # TODO
      compatibility: '0.0.0',
      sizes: [
        { w: 4, h: 4 }
        # TODO: Default size is 4x4, add additional sizes if your widget supports them.
      ],
      # Add all languages for which your Vue templates are fully translated.
      languages: [:enGb, :deDe, :frFr, :esEs, :plPl, :koKr],
      single_source: false # Change to true if your widget doesn't aggregate data from multiple sources.
    }.to_json
  }

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata["allowed_push_host"] = "Set to 'http://mygemserver.com'"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  spec.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  spec.add_dependency "rails", "~> 5.2.6"
end
